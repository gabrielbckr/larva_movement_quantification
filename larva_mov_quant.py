# Needs cv3

import numpy  as np
import cv2    as cv2
import pandas as pd
import sys    as sys
import math   as math

def diffImg(t0, t1, t2):
  d1 = cv2.absdiff(t2, t1)
  d2 = cv2.absdiff(t1, t0)
  return cv2.bitwise_and(d1, d2)

def greater (x, y):
    if x>y:
        return x
    return y

cam = cv2.VideoCapture(sys.argv[1])
winName = sys.argv[2]
flag_show = False
if len(sys.argv)==4:
    if str(sys.argv[3])=='show' :
        flag_show = True
fps = cam.get(cv2.CAP_PROP_FPS)
time_stamp = 0

# Read three images first:
t_minus = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
box = cv2.selectROI("Crop",t_minus,False)
cv2.destroyAllWindows()
t_minus = t_minus[int(box[1]):int(box[1]+box[3]),int(box[0]):int(box[0]+box[2])]
t = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
t = t[int(box[1]):int(box[1]+box[3]),int(box[0]):int(box[0]+box[2])]
t_plus = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
t_plus = t_plus[int(box[1]):int(box[1]+box[3]),int(box[0]):int(box[0]+box[2])]

#box_size = float( box[2] - box[0] ) * ( box[3] - box[1] )

# Starts Dataframe
df_file = pd.DataFrame()

# Get the Petri Plate Diameter
petriLen= cv2.selectROI("Get the Diameter of Petri Plate", t_minus, False) 
cv2.destroyWindow("Get the Diameter of Petri Plate")
petriLen = greater( abs(petriLen[3]-petriLen[1]),abs(petriLen[2]-petriLen[0]))
petriLen = petriLen**2*math.pi/4


s, frame = cam.read()

# https://stackoverflow.com/questions/18954889/how-to-process-images-of-a-video-frame-by-frame-in-video-streaming-using-opencv/19082750#19082750


while s:

    olar  =   diffImg(t_minus, t, t_plus)
    # Show Differential Image
    if flag_show:
        cv2.imshow( winName, olar )

    # Read next image
    t_minus = t
    t = t_plus
    t_plus = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    t_plus = t_plus[int(box[1]):int(box[1]+box[3]),int(box[0]):int(box[0]+box[2])]

    # Show original image
    if flag_show:
        cv2.imshow( "ORIGINAL", t_plus )
        
    # Record Intensity
    frame_intensity = float(sum(sum(olar)) )
    frame_intensity =  frame_intensity / petriLen
    df_file[df_file.size] = pd.Series( {'frame_intensity':frame_intensity,
                                        'time_stamp':time_stamp} )
    time_stamp+=1/fps
    # Close if ESC
    key = cv2.waitKey(10)
    if key == 27:
        cv2.destroyWindow(winName)
        print ("Goodbye")
        break
    s, frame = cam.read()

# Attach Petri Info
#df_file[df_file.size] = df_petri_info
# Export DataFrame
df_file.T.to_csv(winName + '.csv')
cam.release()
